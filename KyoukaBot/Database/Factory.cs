﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.IO;
using System.Text.Json;

namespace KyoukaBot.Database {
    public class KyoukaBotDBContextFactory : IDesignTimeDbContextFactory<KyoukaBotDBContext> {
        public KyoukaBotDBContext CreateDbContext(string[] args)
        {
            string connectionString = JsonSerializer.Deserialize<BotConfig>(File.ReadAllText("config.json")).MySQL.ToString();
            var optionsBuilder = new DbContextOptionsBuilder<KyoukaBotDBContext>();
            optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            return new KyoukaBotDBContext(optionsBuilder.Options);
        }
    }
}
