﻿using Microsoft.EntityFrameworkCore;

namespace KyoukaBot.Database {
    public class KyoukaBotDBContext : DbContext {
        public KyoukaBotDBContext(DbContextOptions<KyoukaBotDBContext> options) : base(options) { }

        public DbSet<ChanPost> ChanPosts { get; set; }
    }
}
