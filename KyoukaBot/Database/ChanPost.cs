﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KyoukaBot.Database {
    public class ChanPost {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [MaxLength(32)]
        public string Username { get; set; }

        [Required]
        [MaxLength(5)]
        public string Board { get; set; }

        public ulong ThreadNumber { get; set; }

        public ulong PostNumber { get; set; }

        [Column(TypeName = "text")]
        public string PostText { get; set; }

        public ulong ImageID { get; set; }

        [MaxLength(5)]
        public string ImageExtension { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime PostTime { get; set; }
    }
}
