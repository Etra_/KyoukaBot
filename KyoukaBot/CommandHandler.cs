﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using KyoukaBot.Database;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Web;
using Webmilio.Commons.FChan;

namespace KyoukaBot {
	public class CommandHandler {
		private readonly BotConfig _config;
		private readonly CommandService _commands;
		private readonly DiscordSocketClient _client;
		private readonly IServiceProvider _services;
		private readonly Dictionary<ulong, int> _channels;
		private readonly Random _random;
		private readonly KyoukaBotDBContext _db;


		public CommandHandler(IServiceProvider services)
		{
			_services = services;
			_commands = _services.GetRequiredService<CommandService>();
			_client = _services.GetRequiredService<DiscordSocketClient>();
			_config = _services.GetRequiredService<BotConfig>();
			_random = _services.GetRequiredService<Random>();
			_db = _services.GetRequiredService<KyoukaBotDBContext>();
			_channels = new();

			_client.MessageReceived += async messageParam => {
				SocketUserMessage message = messageParam as SocketUserMessage;
				if (message == null)
					return;

				int argPos = 0;

				if (!(message.HasStringPrefix(_config.Prefix, ref argPos) || message.Author.IsBot)) {
					if (!message.Author.IsBot) {
						foreach (SocketUser user in message.MentionedUsers) {
							if (user.Id == _client.CurrentUser.Id) {
								await Johnny(message);
								foreach (var channel in _channels) {
									if (message.Channel.Id == channel.Key) {
										_channels[channel.Key] = 0;
										return;
									}
								}
								return;
							}
						}
						foreach (var channel in _channels) {
							if (message.Channel.Id == channel.Key) {
								_channels[channel.Key]++;
								if (channel.Value == _config.PostInterval) {
									await Johnny(message);
									_channels[channel.Key] = 0;
								}
								return;
							}
						}
						_channels[message.Channel.Id] = 1;
					}
					return;
				}

				await _commands.ExecuteAsync(new SocketCommandContext(_client, message), argPos, _services);
			};
		}

		public async Task InitialiseAsync() => await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);

		private async Task Johnny(SocketUserMessage message)
		{
			using (message.Channel.EnterTypingState()) {
				string board;

				if (!_config.ChannelBoards.TryGetValue(message.Channel.Id, out board))
					board = _config.Boards[_random.Next(_config.Boards.Length)];
				
				List<Thread> threads = await FChan.GetThreadsPageless(board);
				ulong threadno = threads[_random.Next(threads.Count)].no;
				await Task.Delay(1000);
				Post[] posts = await FChan.GetPosts(board, threadno);
				Post post = posts[_random.Next(posts.Length)];
				string msg = DeRegex(post.com ?? "<a>");
				if (msg != string.Empty)
					await message.ReplyAsync(msg);
				if (post.tim != null && msg != string.Empty)
					await message.Channel.SendMessageAsync($"https://i.4cdn.org/{board}/{post.tim}{post.ext}");
				if (post.tim != null && msg == string.Empty)
					await message.ReplyAsync($"https://i.4cdn.org/{board}/{post.tim}{post.ext}");

				await _db.AddAsync(new ChanPost {
					Username = message.Author.Username,
					Board = board,
					ThreadNumber = threadno,
					PostNumber = post.no,
					PostText = msg,
					ImageID = post.tim.HasValue ? post.tim.Value : 0,
					ImageExtension = post.ext
				});
				await _db.SaveChangesAsync();
			}
		}

		private string DeRegex(string input)
		{
			string str = Regex.Replace(HttpUtility.HtmlDecode(input), "<br>", "\n");
			str = Regex.Replace(str, "<.*?>", string.Empty);
			str = Regex.Replace(str, "\\[\\/?spoiler\\]", "||");
			str = Regex.Replace(str, ">>>\\/[0-9a-z]+\\/[0-9]+", string.Empty);
			str = Regex.Replace(str, ">>[0-9]+", string.Empty);
			return Regex.Replace(str, "^\n", string.Empty);
		}
	}
}