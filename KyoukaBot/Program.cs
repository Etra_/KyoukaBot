﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using KyoukaBot.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace KyoukaBot {
	public static class Program {
		public async static Task Main() {
			if (!File.Exists("config.json")) {
				await File.WriteAllTextAsync("config.json", JsonSerializer.Serialize(new BotConfig(), new JsonSerializerOptions { WriteIndented = true }));
				Console.Write("A template config file has been created. Fill it out, then press enter to continue.");
				Console.ReadLine();
			}
			BotConfig config = JsonSerializer.Deserialize<BotConfig>(await File.ReadAllTextAsync("config.json"));

			ServiceProvider services = new ServiceCollection()
				.AddSingleton<DiscordSocketClient>()
				.AddSingleton(new CommandService(
					new CommandServiceConfig {
						CaseSensitiveCommands = false,
						DefaultRunMode = RunMode.Async
					}))
				.AddDbContext<KyoukaBotDBContext>(options => options.UseMySql(config.MySQL.ToString(), ServerVersion.AutoDetect(config.MySQL.ToString())))
				.AddSingleton<CommandHandler>()
				.AddSingleton<Random>()
				.AddSingleton(config)
				.AddSingleton(new TenorSharp.TenorClient(config.TenorToken))
				.BuildServiceProvider();

			DiscordSocketClient client = services.GetRequiredService<DiscordSocketClient>();
			client.Log += msg => Console.Out.WriteLineAsync(msg.ToString());

			await services.GetRequiredService<CommandHandler>().InitialiseAsync();
			await client.LoginAsync(TokenType.Bot, config.DiscordToken);
			await client.StartAsync();
			await client.SetGameAsync(config.Game);

			await Task.Delay(-1);
		}
	}
}