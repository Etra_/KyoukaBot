﻿using System.Collections.Generic;

namespace KyoukaBot {
	public record BotConfig {
		public string Prefix { get; init; } = "!";
		public string DiscordToken { get; init; } = "";
		public string TenorToken { get; init; } = "";
		public string Game { get; init; } = "";
		public string[] Boards { get; init; } = { "" };
		public int PostInterval { get; init; } = 10;
		public ConfigSQL MySQL { get; init; } = new();
		public string[] Waifus { get; init; } = { "" };
		public Dictionary<ulong, string> ChannelBoards { get; init; } = new();
	}

	public record ConfigSQL {
		public string Server { get; init; } = "127.0.0.1";
		public int Port { get; init; } = 3306;
		public string Username { get; init; } = "";
		public string Password { get; init; } = "";
		public string Database { get; init; } = "";
		public string Protocol { get; init; } = "socket";
		public override string ToString() => $"server={Server};port={Port};username={Username};password={Password};database={Database};protocol={Protocol}";
	}
}