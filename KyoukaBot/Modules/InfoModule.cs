﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Threading.Tasks;

namespace KyoukaBot.Modules {
	public class InfoModule : ModuleBase<ICommandContext> {
		public DiscordSocketClient Client { private get; init; }
		public CommandService Commands { private get; init; }
		public BotConfig Config { private get; init; }
		public System.Random Rand { private get; init; }

		[Command("ping")]
		[Summary("Gets the ping for the bot.")]
		public async Task Ping() => await Context.Message.ReplyAsync($"Pong! 🏓 Your ping is {Client.Latency}ms.");

		[Command("help")]
		[Summary("DMs you this Help menu.")]
		public async Task Help()
		{
			using (Context.Channel.EnterTypingState()) {
				if (Context.Message.Channel is ITextChannel)
					await Context.Message.ReplyAsync("A help message has been sent to your DMs.");

				EmbedBuilder embed = new();
				embed.Title = $"{Client.CurrentUser.Username} Help Page";
				embed.Color = new Color(0xE1B7CD);
				embed.ImageUrl = Config.Waifus[Rand.Next(Config.Waifus.Length)];
				foreach (var command in Commands.Commands)
					embed.AddField($"`{Config.Prefix}{command.Name}`", command.Summary ?? "No description available.");
				await Context.User.SendMessageAsync(null, false, embed.Build());
			}
		}
	}
}