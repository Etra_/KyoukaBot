using Discord.Commands;
using System;
using System.Threading.Tasks;
using TenorSharp;
using TenorSharp.SearchResults;

namespace KyoukaBot.Modules {
	public class TenorModule : ModuleBase<ICommandContext> {
		public TenorClient Tenor { private get; init; }
		public Random Rand { private get; init; }

		[Command("blm")]
		[Summary("Go Crazy! Go Stupid!")]
		public async Task BLM() => await TenorSearch("nigmode");

		[Command("terry")]
		[Summary("Summon King Terry the Terrible.")]
		public async Task Terry() => await TenorSearch("terry davis");

		[Command("embed")]
		[Summary("Epic embed fail")]
		public async Task Embed() => await TenorSearch("embed fail");

		private async Task TenorSearch(string input)
		{
			using (Context.Channel.EnterTypingState()) {
				Gif result = Tenor.Search(input);
				await ReplyAsync(result.GifResults[Rand.Next(result.GifResults.Length)].ItemUrl.ToString());
			}
			await Context.Message.DeleteAsync();
		}
	}
}
